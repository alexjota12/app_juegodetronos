import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import NavBar from './shared/NavBar/NavBar';
import Home from './pages/Home/Home';
import Casas from './pages/Casas/Casas';
import Personajes from './pages/Personajes/Personajes';
import Cronologia from './pages/Cronologia/Cronologia';



function App() {


  return (


    <Router>

      <NavBar />
      <Switch>
        <Route path="/personajes">
          <Personajes />
        </Route>
        <Route path="/cronologia">
          <Cronologia />
        </Route>
        <Route path="/casas">
          <Casas />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>



  );
}

export default App;
