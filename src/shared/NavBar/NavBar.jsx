import React from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";

export default function NavBar() {
    return (

        <nav className="navbar">
            <Link className="navbar__link" to="/">Home</Link>
            <Link className="navbar__link" to="/personajes">Personajes</Link>
            <Link className="navbar__link" to="/casas">Casas</Link>
            <Link className="navbar__link" to="/cronologia">Cronología</Link>

        </nav>

    )
}
